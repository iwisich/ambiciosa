FROM python:3.6-slim

ENV LANG en_US.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update -qq \
 && apt-get install -qqy \
       gcc \
 && apt-get autoclean \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 && pip install --upgrade pip

COPY requirements.txt /usr/local/ambiciosa/requirements.txt

WORKDIR /usr/local/ambiciosa

RUN pip install -r requirements.txt

COPY start.py /usr/local/ambiciosa/start.py
COPY ambiciosa /usr/local/ambiciosa/ambiciosa
COPY data /usr/local/ambiciosa/data
COPY tests /usr/local/ambiciosa/tests
COPY test_runner.sh /usr/local/ambiciosa/test_runner.sh

CMD ["./ambiciosa/ambiciosa.py"]
