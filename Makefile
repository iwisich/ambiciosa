VERSION := dev
id := ambiciosa

image := $(id):$(VERSION)

DOCKER_TASK = docker run --rm -it

build:
	docker build --tag $(image) .
.PHONY: build

# install:
# 	docker run -d -t --name $(id) \
#              --env-file $(PWD)/.env \
#              $(image)
# .PHONY: install

clean:
	docker rm -vf $(id)
.PHONY: clean

logs:
	docker logs -f $(id)
.PHONY: logs


typecheck: FILE_LIST = $(filter-out %/utils.py, $(wildcard ambiciosa/*.py))
typecheck: EXTRA_FLAGS = $(if $(VERBOSE), --stats)
typecheck:
	@$(DOCKER_TASK) $(image) \
                  mypy --ignore-missing-imports \
                       --follow-imports=skip \
                       $(EXTRA_FLAGS) $(FILE_LIST)
.PHONY: typecheck

lint:
	@$(DOCKER_TASK) $(image) pylint ambiciosa
.PHONY: lint

test:
ifdef JUNIT
	@$(DOCKER_TASK) $(image) ./test_runner.sh
else
	@$(DOCKER_TASK) $(image) python -m pytest -v tests
endif
.PHONY: lint

repl:
	@$(DOCKER_TASK) $(image) ipython
.PHONY: repl

shell:
	@$(DOCKER_TASK) \
		$(image) bash
.PHONY: shell

start:
	@$(DOCKER_TASK) $(image) python start.py
.PHONY: ambiciosa
