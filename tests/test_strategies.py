from ambiciosa.simulator import Player, PlayerList, ScoreBoard, Game
from ambiciosa.strategies import DumbStrategy, RandomStrategy, MinValStrategy



def test_dumb_strategy():
    strat_parameters = {'strategy_name' : 'dumb',
                        'min_val' : 0,
                        'num_rolls' : 0,
                        'composed_strat_name_1' : 'na',
                        'composed_strat_name_2' : 'na',
                        'composed_switch_score' : 0}
    p_1 = Player("Dumb_1", DumbStrategy(strat_parameters))
    pl = PlayerList()
    pl.add(p_1)
    game = Game(pl)
    again = game.player_list.current_player().strategy.play_again(p_1, game)

    assert(again is True)


# def test_continue_min_val20():
#     strategy = st.MinValStrategy(20)
#     player_list = PlayerList()
#     player = Player("Diana", strategy)
#     player_liadd(player)
#     game = Game(player_list)
#     score = 30
#     turns = 1
#     turn = game.continue_turn(player_liplayers[0], score, turns)
#     assert(turn == False)


