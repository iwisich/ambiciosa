from ambiciosa.simulator import Dice, Player, PlayerList, ScoreBoard, Game, AmbiciosaTUI
from ambiciosa.simulator import AmbiciosaScorer
from ambiciosa.strategies import DumbStrategy, RandomStrategy, MinValStrategy

strat_parameters = {'strategy_name' : 'random',
                    'min_val' : 0,
                    'num_rolls' : 0,
                    'composed_strat_name_1' : 'na',
                    'composed_strat_name_2' : 'na',
                    'composed_switch_score' : 0}
p_1 = Player("Random_1", RandomStrategy(strat_parameters))
p_2 = Player("Random_2", RandomStrategy(strat_parameters))


# Test Dice
def test_dice_roll():
    dau6 = Dice(6)
    result = dau6.roll()

    assert(result >= 1 and result <= 6)



# Test Player
def test_player_play_again():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    play_again = p_1.play_again(game)

    assert(play_again == True or
           play_again == False)


def test_player_continue_turn():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    game.board.add_score(p_1, 10)
    continue_turn = p_1.continue_turn(game)

    assert(continue_turn == True or continue_turn == False)

    game.board.add_score(p_1, 1000)
    continue_turn = p_1.continue_turn(game)

    assert(continue_turn == False)


def test_player_play_turn():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    score = p_1.play_turn(game)
    assert(score >= 0)



# Test PlayerList
def test_player_list_add():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)

    assert(pl.current_player() == p_1)

    next(pl)
    assert(pl.current_player() == p_2)


def test_player_list_next():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    next(pl)

    assert(pl.index == 1)
    next(pl)

    assert(pl.index == 0)


def test_player_list_current_player():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)

    assert(pl.current_player() is p_1)

    next(pl)
    assert(pl.current_player() is p_2)


def test_player_list_len():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)

    assert(len(pl) is 2)


def test_player_list_to_list():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)

    assert(pl.to_list() == pl.players)



# Test ScoreBoard
def test_score_board_add_score():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    sb = ScoreBoard(pl)

    sb.add_score(p_1, 10)
    score_table = {p_1 : [10],
                   p_2 : []}

    assert(sb.score_table == score_table)


def test_score_board_total_score():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    sb = ScoreBoard(pl)

    sb.add_score(p_1, 10)
    sb.add_score(p_1, 20)
    sb.add_score(p_1, 20)
    sb.add_score(p_1, 100)

    assert(sb.total_score(p_1) == 150)


def test_score_board_max_score():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    sb = ScoreBoard(pl)

    sb.add_score(p_1, 10)
    sb.add_score(p_1, 20)
    sb.add_score(p_1, 20)
    sb.add_score(p_2, 100)

    assert(sb.max_score() == 100)


def test_score_board_best_player():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    sb = ScoreBoard(pl)

    sb.add_score(p_1, 10)
    sb.add_score(p_1, 20)
    sb.add_score(p_1, 20)
    sb.add_score(p_2, 100)

    assert(sb.best_player() == p_2)



# Test scorer
def test_ambiciosa_scorer_count_usual():
    scorer = AmbiciosaScorer()

    assert(scorer.count_usual(1) == 10)
    assert(scorer.count_usual(2) == 5)
    assert(scorer.count_usual(3) == 0)
    assert(scorer.count_usual(6) == 0)


def test_ambiciosa_scorer_roll_ambiciosa():
    scorer = AmbiciosaScorer()

    for repetition in range(100):
        outcome = scorer.roll_ambiciosa()
        assert(outcome[0] >= 1 and outcome [0] <= 6)
        assert(outcome[1] >= 1 and outcome [1] <= 6)
        assert(outcome[2] >= 1 and outcome [2] <= 6)


def test_ambiciosa_scorer_count_ambiciosa():
    scorer = AmbiciosaScorer()
    assert(scorer.count_ambiciosa([ 1, 1, 3]) == 20)
    assert(scorer.count_ambiciosa([ 3, 4, 1]) == 10)
    assert(scorer.count_ambiciosa([ 6, 2, 2]) == 10)
    assert(scorer.count_ambiciosa([ 6, 4, 3]) == 0)
    assert(scorer.count_ambiciosa([ 1, 1, 1]) == 100)
    assert(scorer.count_ambiciosa([ 2, 2, 2]) == 50)
    assert(scorer.count_ambiciosa([ 6, 6, 6]) == -30)
    assert(scorer.count_ambiciosa([ 5, 5, 5]) == -10)



# Test Game
def test_game_is_over():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    assert(game.is_over() is False)

    game.board.add_score(p_1, 510)
    assert(game.is_over() is True)


def test_game_winner():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    assert(game.winner() is None)

    game.board.add_score(p_1, 510)
    assert(game.winner() is p_1)


def test_game_play_game_winner_300():
    pl = PlayerList()
    pl.add(p_1)
    pl.add(p_2)
    game = Game(pl)

    game.play_game()
    assert(game.board.total_score(game.current_player()) >= game.winning_score)

