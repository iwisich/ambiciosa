# L'ambiciosa simulator

Simulator of the dice game: _L'ambiciosa_.

The [rules](doc/rules.md).

## Goal
The simulator will eventually allow the user to choose among some strategies
for a few players and allow them to play against each other a number of times
and report stats to compare the strategies.

See the [strategies document](doc/strategies.md) for all the strategies that
are currently planned to be developed.

## Status
WIP

Master is functional but limited to 4 possible strategies and little
functionality.

## How to use it

### Players set up
Currently the best way to set up players is to modify the `data/players.csv`
where each line under the headings corresponds to a player.

The fields are:
  - player_name: any string with no commas;
  - strategy_name: unquoted name, the current options are:
    + dumb;
    + random;
    + min_val;
    + min_score;
    + switch_score;
    + be_ahead.
  - min_val: when the _min_val_ strategy is used, this field has the minimum
      value as an integer.  When it's not used, any integer is valid as a
      placeholder;
  - num_rolls: when the _num_rolls_ strategy is used, this field has the
      max number of rolls per turn. When it's not used, any integer is valid as
      a placeholder;
  - composed_strategy_1: when the _switch_score_ strategy is used, the first
      strategy name used is included here as it would be in the strategy_name
      field. Any parameters must be included in the relevant place.
      _Note: this needs massive improvements to make it more flexible_;
  - composed_strategy_2: as the previous one but for the second strategy;
  - composed_switch_score: the score that would determine a change in strategy
      considering the _switch_strategy_.

### Running the simulator
The easiest way of running the simulator is via Docker and Make.

  - `make build` builds the Docker image. Note that it is needed every time
      that you update the `data/players.csv` as the data needs to be copied
      into the container;
  - `make start` will run the TUI and provide two options: (a) to run the player
list from `data/players.csv`, and (b) to run a default player list;
  - `make test` runs the tests;


## LICENCE
MIT
