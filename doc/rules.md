# L'ambiciosa - game rules

## Required material
Three poker six faces dice.

Faces can be:
  - Ace (A);
  - King (K);
  - Queen (Q);
  - Jack (J);
  - Nine (9);
  - Eight (8).

## Game sequence
The first player is selected at random.

Players play in a cyclic sequence.

The first player rolls the dice with two possible outcomes:

  1. The player's score is positive;
  2. The player's score is equal or lower than 0.


If 2 then the player gets that score for the turn and it's the next player's
turn.

If 1, then the player can decide to either keep that score and give the turn to
the next player or to roll the dice again. If she rolls the dice, the above two
options are again possible.

If 2 then the player gets that score for the turn without accumulating any
other score in the turn and it's the next player's turn.

If 1, then the player adds that score to the current turn score and decides to
either keep it and pass the turn to the next player or to roll the dice again.

This way one's turn lasts until either you get a zero or below score or until
you decide to keep your accumulated score.


## Scores
Only aces (A) and kings (K) score positive points:
  - Each A is worth 10 points;
  - Each K is worth 5 points.

In addition to that there are special outcomes:
  - Three As are worth 100 points;
  - Three Ks are worth 50 points;
  - Three 9s are worth -10 points;
  - Three 8s are worth -30 points.

Alternative rules also can include variations of the following:
  - Three Qs are worth -10 points;
  - Three Js are worth -30 points;
  - Three 9s are worth -50 points;
  - Three 8s are worth -100 points.


## Game end
A game ends when a player reaches 200 points.

Possible variations include:
  - ending the round of players to allow all players
to have the same number of turns;
  - using different winning points e.g. 500


## Example game situation
A game with two players:

1. Player 1 rolls a 15 (A, K, J)
2. Player 1 decides to roll again and gets 20 (A, K, K)
3. Player 1 decides to keep the score (35 = 15 + 20) and pass the turn to
   Player 2.
4. Player 2 rolls a 100 (A, A, A)
5. Player 2 decides to roll again and gets a 0
6. Player 2's turn is over and gets 0 points
7. Player 1 rolls a 10 (A, Q, Q)
8. Player 1 decides to keep the 10 points, which are added to his total score
   of 35, so his total score is now 45, and passes the turn to Player 2
9. Player 2 rolls the dice ... etc.



