# Ambiciosa design

### Definition
A **strategy** is an object with a single method `play_again`. Which depends on
a configuration and information from `game` of type `Game`.

The method `play_again` is the algorithm that provides an answer to whether or
not a `Player` with that strategy will play again in a given turn.


### Definition
Let the two **basic strategies** `AlwaysStrategy` and `NeverStrategy`. be
defined by the following `play_again` methods:

  - `AlwaysStrategy.play_again(game)` is always `True` for any `game`;
  - `NeverStrategy.play_again(game)` is always `False` for any `game`.

where `game` is a `Game`.


### Definition
Let a **switch** be a function `sw` defined by:

    sw:  S1 x S2 x P(Game)   ->   S3
         s1,  s2,  p(game)        s3 = sw(s1, s2, p)

where `si` are strategies and `p` is a function that determines the probability
of choosing the first one.

`s3`'s `play_again` method will be defined by:

    s3.play_again(game) = s1.play_again(game) with probability p(game)
    s3.play_again(game) = s2.play_again(game) with probability 1 - p(game)


### Definition
Let a **composition** be a function `comp`

    comp:  S1 x S2 -> S3
           s1,  s2    s3 = comp(s1, s2)

where `si` are strategies.

`s3`'s `play_again` method will be defined by:

    s3.play_again(game) = s1.play_again(game) if s2.play_again(game) was True
    and False otherwise


### Proposition
All strategies can be expressed as a sequence of combinations of **switch** and/or **composition** of the two **basic strategies**.

---

### Examples
Given `a = AlwaysStrategy`, `n = NeverStrategy`:

  - `DumbStrategy := sw(a, n, 1)`
  - `RandomStrategy := sw(a, n, 1/2)`
  - `MinScoreStrategy := sw(s1, s2, p)` where `s1` and `s2` are strategies
      built with **the basic strategies**, `sw` and/or `comp`. Where `p = 1` if `turn_score > k` and `p = 0` otherwise. For `k` an integer `k >= 0` and `k <= WINNING_SCORE`
  - `MinValStrategy := sw(s1, s2, p)` where `s1` and `s2` are strategies
      built with **the basic strategies**, `sw` and/or `comp`. Where `p = 1` if
      `number_of_rolls <= k`, with `k` a positive integer.


### Proof

---

---

## Classes
### Player List
A `player_list` should know:
  - What players it contains
  - How to add a player to it
  - Which player's turn it is
  - How to move to the next player
  - How many players are on the list


### Game
A `game` should know:
  - who's turn it is
  - if there is a winner in the game
  - who the winner of the game is
  - who the players of the game are
  - if the game is over
  - how many turns have been played

A `game` is dependant on a specific `player_list`


### Score board
A `score_board` should know:
  - what players are in the scoreboard
  - what scores have each player made for each of their turns
  - how to calculate what the total score is for a specific player
  - how to add a new score to the board for a player in it

A `score_board` depends on a specific `game`.


### Player -> rename to TurnRunner?
A `player` has the following properties:

  - game_points: integer (0)
  - playing: boolean (false)
  - strategy: string -- it could become more complicated (strat[i])
  - continue_turn: boolean (false)
  - turn_points: integer (0)
  - number_of_games_won: natural (0)
  - turn: boolean -- on/off (off)

## Understanding a strategy

### Definition
A strategy is a process or algorithm that, based on some available information, outputs a yes/no answer to the question "do I
keep rolling the dice?"

### Drivers
The input information is anything that can be obtained from the game that's being
played, or parameters that have been built based on historical games.

Inputs include:

  _Overall_

  - Minimum score to win (can be considered a constant);
  - Number of players in the game;

  _Current player_

  - Current points in turn;
  - Current total score of current player;
  - Number of times the current player has rolled in this game;
  - Number of times the current player has rolled in this turn;
  - Number of turns so far;

  _Other players_

  - Current total score of other players;
  - Number of times other players have rolled;

### Modelling
The process of playing again can be modelled as a deterministic or as a
stochastic process.

For example, a logistic regression can be built based on the input parameters
to forecast the response. Which can then be estimated based on a deterministic
or a stochastic process.

Building the model can be done with sophisticated statistics or machine
learning methods or simplisticly based on experience or imagination.


### Parametrisation

On top of (some of) the list of inputs in the list above, a strategy needs a
description of the algorithm to use them.

Combined parameters can be seen as the answers to the following questions:

  - is the turn score above a certain threshold?
  - is the game score above a certain threshold (it could be a function of
      other players' scores and the minimum score to win the game)?
  - was the previous turn score below a certain threshold?
  - is the number of times the player has rolled more than a certain threshold?
  - is the average score per turn below a certain threshold?

---




































