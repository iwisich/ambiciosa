# List of possible strategies:

Any strategy is the way in which a player decides whether or not to play again
after each turn.

## Independent
Strategies that don't depend on game information.

### Random
The decision is random. There is a 50% chance to play again.
This strategy can be extended by parametrising the chance to play again, and
choose a different probability.

### Dumb
The player always chooses to play again.

## Individual
Strategies that only depend on the player's own information.

### Minimum value
The player plays again as long as a minimum value hasn't been surpassed.
This strategy requires the threshold value and should be parametrised.

### Number of turns
The player plays again until he's reached a given number of turns.
This is a group of strategies depending on the number of turns.

### Close to win
The player has a strategy S up until she reaches a threshold that relates to
the points to get to win. At that point the player uses a different strategy
(the Dumb one?)

## Competition
Strategies that depend on the other players' information.

### Be ahead
The player plays again as long as she's not ahead of all other players in the
game.

### Copycat 1
The player tries to play as many turns as the previous player.

### Copycat 2
The player tries to make as many points as the previous player.

### Adapt by distance
The player changes strategy depending on the distance in points to other
players. e.g. changes to dumb if he falls behind by n points.

### Adapt by close to win
The player changes strategy if any other player is at a distance of n points to
win.
This is a group of strategies.
