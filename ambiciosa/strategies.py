import random

class DumbStrategy:
    """Strategy objects that have a minimal strategy: if possible keep playing
    until the player wins or the turn ends"""

    def __init__(self, _strat_parameters):
        self.name = "dumb"

    def play_again(self, _player, _game):
        return True


class RandomStrategy:
    """Strategy objects that have a random strategy: there is a 0.5 chance of
    playing again"""

    def __init__(self, _strat_parameters):
        self.name = "random"


    def play_again(self, _player, _game):
        return random.choice([True, False])


class MinValStrategy:
    """Strategy objects which play again depending on a minimum score
    achieved in that rolling round"""

    def __init__(self, strat_parameters):
        self.name = "min_val"
        self.min_val = strat_parameters['min_val']


    def play_again(self, player, _game):
        return player.turn_score < self.min_val


class NumRollsStrategy:
    """Strategy objects which will play again if a number of rolling rounds has
    been done"""

    def __init__(self, strat_parameters):
        self.name = "num_rolls"
        self.num_rolls = strat_parameters['num_rolls']


    def play_again(self, player, _game):
        return player.turn_rolls < self.num_rolls


class SwitchScoreStrategy:
    """Strategy objects which will use an initial strategy and switch to a
    different one at a after a certain threshold score is reached """

    def __init__(self, strat_parameters):
        self.name = "switch_score"
        self.strat_parameters = strat_parameters


    def play_again(self, player, game):
        current_total_score = game.board.total_score(player) + player.turn_score

        if current_total_score > self.strat_parameters['composed_switch_score']:
            current_strat = DumbStrategy(self.strat_parameters)
        else:
            current_strat = MinValStrategy(self.strat_parameters)

        return current_strat.play_again(player, game)


class BeAheadStrategy:
    """Strategy objects which will keep on rolling if she's behind other
    players' max score"""
    def __init__(self, _strat_parameters):
        self.name = "be_ahead"


    def play_again(self, player, _game):
        current_total_score = game.board.total_score(player) + player.turn_score
        current_max_score = game.board.max_score()

        if current_max_score > player.current_total_score:
            return True

        return False

