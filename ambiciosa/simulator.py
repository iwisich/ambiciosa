# l'ambiciosa simulator
""" L'ambiciosa dice game simulator

This module simulates l'ambiciosa game with several different strategies.
The aim is to assess which strategies are better.
"""


import os
from random import randint
import itertools
import csv
import ambiciosa.strategies as st


class Dice:
    """Simulate a dice with the number of sides."""

    def __init__(self, sides):
        self.sides = sides


    def roll(self):
        """Roll a die"""

        return randint(1, self.sides)


class Player():
    """Run the turn for a player.

    Use its strategy and follow l'ambiciosa game rules.
    """

    def __init__(self, name, strategy):
        self.name = name
        self.strategy = strategy
        self.__reset_turn_vars()

    def __reset_turn_vars(self):
        """Reset turn variables."""
        self.turn_score = 0
        self.turn_rolls = 0

    def play_again(self, game):
        """Wrap the play_again method corresponding to each strategy.

        Note that `strategy.play_again` will use the strategy belonging
        to the current player.
        """
        return self.strategy.play_again(self, game)

    def continue_turn(self, game):
        """Decide whether or not to play again.

        The decision is based on having reached the winning score or on the
        current player strategy.
        See the `play_again` method.
        """
        current_total_score = game.board.total_score(self)

        # if the turn score and the current cummulative score reaches the
        # winning score the player will definitely stop as she'll have won
        if self.turn_score + current_total_score >= game.winning_score:
            return False

        # use the strategy to decide whether or not to play again
        return self.play_again(game)

    def play_turn(self, game):
        """Play a turn for a player given its strategy and the information from
        the game
        """
        continue_turn = True  # Note that the first time a player always rolls

        while continue_turn:
            roll = game.scorer.roll_ambiciosa()
            extra_score = game.scorer.count_ambiciosa(roll)
            self.turn_rolls += 1

            if extra_score <= 0:
                self.__reset_turn_vars()
                return extra_score

            self.turn_score += extra_score
            continue_turn = self.continue_turn(game)

        # Reset turn parameters
        turn_score = self.turn_score
        self.__reset_turn_vars()

        return turn_score


class PlayerList:
    """Store a list of players.

    These objects are built as cyclic iterators.
    """

    def __init__(self):
        self.players = []
        self.index = 0

    def add(self, player):
        """Add a player to the list."""
        self.players.append(player)

    def __next__(self):
        """Iterate to the next player in the list.

        After the last element it goes back to the first.
        """
        next_index = self.index + 1

        if next_index >= len(self):
            self.index = 0
        else:
            self.index = next_index

        return self.current_player()

    def to_list(self):
        """Convert a player_list into a list."""
        return self.players

    def current_player(self):
        """Get the current player in the list.

        i.e. the one corresponding to the index.
        """
        return self.players[self.index]

    def __len__(self):
        """Get the size of the players_list."""
        return len(self.players)

    # def all(self):
    #     return self.players


class ScoreBoard:
    """Keep track of the scores of players from a game."""

    def __init__(self, player_list):
        self.score_table = dict((player, []) for player in player_list.to_list())

    def add_score(self, player, score):
        """Add the score from a turn to a player."""

        self.score_table[player].append(score)

    def total_score(self, player):
        """Calculate the current total score for a player."""

        return sum(self.score_table[player])

    def max_score(self):
        """Calculate the maximum score from all players."""
        max_score = 0

        for player in self.score_table:
            total_score = self.total_score(player)

            if  total_score >= max_score:
                   max_score = total_score

        return max_score

    def best_player(self):
        """Identify the player with the maximum score."""
        max_score = 0

        for player in self.score_table:
            total_score = self.total_score(player)

            if  total_score >= max_score:
                   max_score = total_score
                   best_player = player

        return best_player


class AmbiciosaScorer:
    """Score a turn of an ambiciosa game given those rules."""

    def __init__(self):
        self.rules = "ambiciosa"
        self.scores = {
            'ace_1' : 10,
            'king_2' : 5,
            'others' : 0,
            'three_aces' : 100,
            'three_kings' : 50,
            'three_nines' : -10,
            'three_eights' : -30
        }

    def count_usual(self, value):
        """Score l'ambiciosa.

        That is excluding the special scores for three of a kind 1 is Ace, 2
        is King.
        """
        options = {
            1 : self.scores['ace_1'],
            2 : self.scores['king_2']
        }

        return options.get(value) or self.scores['others']

    def roll_ambiciosa(self):
        """"""
        dice_6 = Dice(6)
        return [dice_6.roll(), dice_6.roll(), dice_6.roll()]

    def count_ambiciosa(self, ambiciosa_score):
        """Roll the three dice according to ambiciosa's rules.

        Note that Ace is 1, King is 2, 9 is 5 and 8 is 6
        """
        score = 0

        if max(ambiciosa_score) == 1:
            score = self.scores['three_aces']
        elif max(ambiciosa_score) == 2 and min(ambiciosa_score) == 2:
            score = self.scores['three_kings']
        elif min(ambiciosa_score) == 6:
            score = self.scores['three_eights']
        elif min(ambiciosa_score) == 5 and max(ambiciosa_score) == 5:
            score = self.scores['three_nines']
        else:
            score += self.count_usual(ambiciosa_score[0])
            score += self.count_usual(ambiciosa_score[1])
            score += self.count_usual(ambiciosa_score[2])

        return score


class Game:
    """Objects to manage a game, who's turn it is, score board, etc."""

    def __init__(self, player_list):
        self.player_list = player_list
        self.board = ScoreBoard(player_list)
        self.scorer = AmbiciosaScorer()
        self.winning_score = 300

        # self.current_rolls = 0
        # self.current_score = 0

    def is_over(self):
        """Method to check if there is a winner"""
        return self.board.max_score() >= self.winning_score

    def winner(self):
        """Method to extract a winner if there is one or None if there isn't
        one
        """
        if not self.is_over():
            return None

        return self.board.best_player()

    def play_game(self):
        """This game method plays a game given a list of players in the game"""
        # the game needs to be passed as a parameter because its information
        # may be necessary for the player to decide how to play
        score = self.current_player().play_turn(self)
        self.board.add_score(self.current_player(), score)
        turns = 1

        while not self.is_over():
            next(self.player_list)

            score = self.current_player().play_turn(self)
            self.board.add_score(self.current_player(), score)
            turns += 1

        ## this should return the current_player and maybe the game properties
        return self.current_player().name, turns

    def current_player(self):
        return self.player_list.current_player()


class AmbiciosaTUI:
    """Interact with ambiciosa Game.

    Set up game. Make questions and record the answers
    """

    def __init__(self):
        self.game_name = "ambiciosa"
        self.player_list = None

    def choose_strategy(self, strat_parameters):
        """Select the strategy to a player given the input parameters."""
        name = strat_parameters['strategy_name']

        s = {
          "min_val": st.MinValStrategy,
          "random": st.RandomStrategy,
          "dumb": st.DumbStrategy,
          "num_rolls": st.NumRollsStrategy,
          "switch_score": st.SwitchScoreStrategy,
        }

        strategy = s.get(name) or st.RandomStrategy

        return strategy(strat_parameters)

    def choose_player_list(self):
        """Display the selection of play_list options"""
        os.system("clear")
        print("(a) Get player list from file (players.csv)\n")
        print("(b) Default player list\n")
        print("(c) Stop\n")

        return input()

    def read_player_list_from_file(self, filepath):
        """Method to read a list of players from a file"""
        # run from data/players.csv
        with open(filepath, 'r') as csvfile:
            players_dataset = csv.reader(csvfile, delimiter=',')
            next(players_dataset)  # skip first line of headings

            for record in players_dataset:
                strat_parameters = {
                    'strategy_name' : record[1].strip(),
                    # This fails if the value is not an integer.
                    'min_val' : int(record[2].strip()),
                    'num_rolls' : int(record[3].strip()),
                    'composed_strat_name_1' : record[4].strip(),
                    'composed_strat_name_2' : record[5].strip(),
                    'composed_switch_score' : int(record[6].strip())}

                strategy = self.choose_strategy(strat_parameters)
                player = Player(record[0], strategy)
                self.player_list.add(player)

    def set_default_list(self):
        """Method to input hard-coded default players"""

        strat_parameters = {'strategy_name' : 'min_val',
                            'min_val' : 30,
                            'num_rolls' : 0,
                            'composed_strat_name_1' : 'na',
                            'composed_strat_name_2' : 'na',
                            'composed_switch_score' : 0}

        p_1 = Player("Min_20_Strategy", st.MinValStrategy(strat_parameters))
        p_2 = Player("Min_30_Strategy", st.MinValStrategy(strat_parameters))

        self.player_list.add(p_1)
        self.player_list.add(p_2)

    def set_player_list(self):
        """Select player list."""
        choice = self.choose_player_list()
        # This resets the player_list in case a player plays more than once
        self.player_list = PlayerList()

        options = {'a' : lambda: self.read_player_list_from_file('data/players.csv'),
                   'b' : lambda: self.set_default_list()}

        option = options.get(choice)

        if option is not None:
            return option()

    def get_number_of_games(self):
        """Define the number of games."""
        return 1000

    def simulate_games(self, player_list, number_of_games):
        """Run the simulation of a number of games for a given player list."""
        counter = dict((player.name, 0) for player in player_list.players)
        sum_of_turns = 0

        for simulation in range(number_of_games):
            game = Game(player_list)
            winner, turns = game.play_game()
            counter[winner] += 1
            sum_of_turns += turns

        return counter, sum_of_turns / (number_of_games * len(player_list))

    def display_results(self, outcome):
        """Display the results of the simulation."""
        os.system("clear")
        print(outcome)

    def start(self):
        """Simulate n games of l'ambiciosa and display the results."""
        while True:
            self.set_player_list()

            if self.player_list.players == []:
                break

            number_of_games = self.get_number_of_games()
            outcome = self.simulate_games(self.player_list, number_of_games)
            self.display_results(outcome)
            input()
