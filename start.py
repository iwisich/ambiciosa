#! /usr/bin/env python
"""Main creates and starts the application UI and keeps it running"""

from ambiciosa.simulator import AmbiciosaTUI

# Main
if __name__ == '__main__':
    # Create TUI
    tui = AmbiciosaTUI()
    tui.start()
